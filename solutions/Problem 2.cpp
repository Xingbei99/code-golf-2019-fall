#include <iostream>

int main() {
    oddOnes();
    return 0;
}

void oddOnes(){
    for (int i = 1; i <= 50; i++){
        if (numOnes(i) % 2 == 1)
            std::cout<<std::to_string(i)<<std::endl;
    }
}

int numOnes(int number){
    int numDigits = 0;

    while (number >= 2){
        int curDigit = number % 2;
        if (curDigit == 1)
            numDigits++;
        number = number / 2;
    }

    if (number == 1)
        numDigits++;

    return numDigits;
}